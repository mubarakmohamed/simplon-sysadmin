# Hacking 

### Contre-hacker 🕵
Utiliser `htop` pour détecter la présence du hacker et stopper sa connexion.  
**login: `simplon` mdp: `simplon`**

### Le hacker 💣
Se connecter à la machine d'une autre équipe et ajouter un petit mot dans son `HOME`  
**login: `hacker` mdp: `hacker`**

# Lancer Symfony demo sur un serveur 

## Étape 1 - Installation
Passer en utilisateur root (admin) : `sudo su`
- Installation des deps  
  `apt install apache2 libapache2-mod-php mysql-server php-mysql php-zip php-pdo-sqlite php-mbstring php-xml composer` 

- Installation du projet symfony    
  `composer create-project symfony/symfony-demo demo`

- Vérifier qu'il est possible de lancer le projet en lancant  
  `bin/console server:run`  

- Rendez accessible le **site online** en executant  
  `bin/console server:run 0.0.0.0:8080`  

A cette étape vous devrez pouvoir accéder au site via l'url suivante : [http://MON_IP:8000](http://MON_IP:8000) 👌👌

## Étape 2 - Configuration de Mysql
Commencer par changer la configuration de symfony, pour cela il faut éditer le fichier `.env` et remplcer la ligne :  `DATABASE_URL=sqlite:///%kernel.project_dir%/data/database.sqlite` par `DATABASE_URL=mysqli://localhost:3306/demo`

Puis exécuter les commandes suivantes : 
```bash
bin/console doctrine:database:create # Création de la db
bin/console doctrine:schema:create # Création du schema
bin/console doctrine:fixtures:load # Chargement des données de demo (sinon on a une db vide)
```

Vérifier que les tables ont bien été créé avec `echo "SHOW TABLES;" | mysql demo`  
- Avec cette même methode essayer de voir le contenu de la table des utilisateurs `symfony_demo_user`  
->Un indice ça commence par `SELECT` 😉

## Étape 3 - Configuration d'Apache
Avant de commencer vérifier que apache répond bien sur cette url [http://MON_IP](http://MON_IP)  

Essayer de rendre son site accèsible via apache en suivant cette documentation : 
- Installer un vhost https://doc.ubuntu-fr.org/lamp#creation_d_un_hote_virtuel
- https://symfony.com/doc/current/setup/web_server_configuration.html#adding-rewrite-rules