# Docker

## Tuto docker 
1. https://training.play-with-docker.com/beginner-linux/
2. https://training.play-with-docker.com/docker-volumes/

## Utiliser docker pour lancer le projet symfony demo
1. Lancer `docker run --name app --rm -d -p 80:2015 florianperrot/symfony:4.x`  
Vérifier que le site est accessible sur l'url `eX.simplon.distil.lat` (remplacer `X` par le numéro de la machine)  
Vous devrez avoir une erreur si vous voulez accéder à `eX.simplon.distil.lat/fr/blog/` c'est normal car il n'y a pas encore de DB.

2. Lancer une DB
`docker run --name mysql -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -d mariadb`  

3. Connecter les 2 containers
pour cela on va créer un réseau virtuel avec  
```bash
docker network create demo # notre réseau s'appel `demo` 
docker network connect demo app # On connect notre container app au réseau
docker network connect --alias mon-mysql demo mysql # On connect notre container mysql au réseau et on le rend accessible à l'app grace à un alias qui est `mon-mysql`
```

4. Changer la configuration le projet symfony pour utiliser le container mysql dans le fichier `.env`  
`DATABASE_URL=mysql://mon-mysql:3306/demo`

5. Création de la DB et des données de test  
Accéder à l'interieur du container `demo` le container qui execute l'application symfony avec    
`docker exec -it demo sh` puis lancer :   
```sh 
bin/console doctrine:database:create # Création de la db
bin/console doctrine:schema:create # Création du schema
bin/console doctrine:fixtures:load # Chargement des données de demo (sinon on a une db vide)
```

5. Vérifier que le blog fonctionne `eX.simplon.distil.lat/fr/blog/`

6. Passer de mariaDB à Postgresql 🙄

## Resources
- [Docker doc](https://docs.docker.com/) Documentation de Docker
- [Dockerhub](https://hub.docker.com/search/?type=image) Liste des images disponibles
- [Training docker](https://training.play-with-docker.com/) Liste des tutos officiel
- [Play with docker in browser](https://labs.play-with-docker.com/) Utiliser docker dans le browser